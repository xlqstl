#include "stdexcept"

namespace std {

logic_error::logic_error(const string &what_arg):
    what_(what_arg)
{
}

logic_error::~logic_error() throw()
{

}

const char *logic_error::what() const throw()
{
    return what_.c_str();
}

out_of_range::out_of_range(const string &what_arg):
    logic_error(what_arg)
{
}

}
