#!/bin/sh

build_fail(){
    echo "Build failed"
    exit $1
}

mkdir -p tmp
cp SConstruct main.cxx tmp/

# test with standard library
cd tmp/
scons test_ext=false || build_fail $?
valgrind -q --leak-check=full --show-reachable=yes ./test

# test with our own
cd ../
scons test_ext=true || build_fail $?
valgrind -q --leak-check=full --show-reachable=yes ./test
