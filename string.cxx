#include "string"
#include "stdexcept"

namespace std {

template <class T>
void basic_string<T>::throw_exc()
{
    throw out_of_range("string index out of range");
}

}
