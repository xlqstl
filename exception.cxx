#include "exception"

namespace std {

exception::exception() throw() { }
exception::exception(const exception &) throw() { }
exception &exception::operator = (const exception &) throw() { return *this; }
exception::~exception() throw() { }
const char *exception::what() const throw() { return "exception"; }

}
