#include "new"
#include "cstdlib"

namespace std {

const nothrow_t nothrow = {};

const char *bad_alloc::what()
{
    return "memory full";
}

}

static void *base_new(std::size_t size)
{
    void *ptr;
    ptr = std::malloc(size);
    if (ptr){
        return ptr;
    } else {
        throw std::bad_alloc();
    }
}

void *operator new (std::size_t size)
{
    return base_new(size);
}

void *operator new[] (std::size_t size)
{
    return base_new(size);
}

void operator delete (void *p) throw()
{
    free(p);
}

void operator delete[] (void *p) throw()
{
    free(p);
}

void *operator new (std::size_t, void *p) throw()
{
    return p;
}

void *operator new[] (std::size_t, void *p) throw()
{
    return p;
}

void *operator new (std::size_t size, std::nothrow_t) throw()
{
    return std::malloc(size);
}

void *operator new[] (std::size_t size, std::nothrow_t) throw()
{
    return std::malloc(size);
}
