#include "vector"
#include "list"
#include "cstdio"
#include "cstdarg"
#include "stdexcept"
#include "limits"
#include "type_traits"

static int test_counter = 0,
           fail_counter = 0;

#define TEST(x) test(__FILE__, __LINE__, #x, x)

#define TEST_SEQ(...) test_seq(__FILE__, __LINE__, __VA_ARGS__)

void test_fail(const char *file, int line, const char *what)
{
    printf("\x1B[0;31m%s:%d: test failed: %s\x1B[0m\n", file, line, what);
    ++::fail_counter;
}

void test(const char *file, int line, const char *expr, bool result)
{
    ++test_counter;
    if (!result){
        test_fail(file, line, expr);
    }
}

template <class T>
void test_seq(const char *file, int line, const T &v, int n, ...)
{
    va_list ap;
    ++test_counter;
    va_start(ap, n);
    if (n == v.size()){
        for (typename T::const_iterator
          i=v.begin(); i!=v.end(); ++i){
            if (*i != va_arg(ap, typename T::value_type)){
                test_fail(file, line, "TEST_SEQ");
                break;
            }
        }
    } else {
        test_fail(file, line, "TEST_SEQ (sequence size)");
    }
    va_end(ap);
}


template <class T>
void dump_seq(const T &v)
{
    for (auto i = v.begin(); i!=v.end(); ++i){
        // TODO: use iostream
        printf("%d, ", static_cast<int>(*i));
    }
    printf("\n");
}

struct MyStruct {};

struct MyNonPod: public MyStruct {
    MyNonPod(){}
};

struct MyMove {
    int x;

    MyMove()
    {
        x = 0;
    }

    MyMove(MyMove &&other)
    {
        *this = std::move(other);
    }

    MyMove &operator = (MyMove &&other)
    {
        x = other.x;
        other.x = 0;
        return *this;
    }
};

int main()
{
    // these are based on x86/x86_64!
    TEST(std::numeric_limits<int>::max() == 2147483647);
    TEST(std::numeric_limits<int>::min() == -2147483648);
    TEST(std::numeric_limits<int>::digits == 31);
    TEST(std::numeric_limits<unsigned int>::digits == 32);
    TEST(std::numeric_limits<int>::digits10 == 9);
    TEST(std::numeric_limits<bool>::digits == 1);
    TEST(std::numeric_limits<bool>::digits10 == 0);

    TEST((std::is_same<int, int>::value));
    TEST((!std::is_same<int, unsigned int>::value));
    TEST(!std::is_integral<float>::value);
    TEST(std::is_integral<bool>::value);
    TEST(!std::is_scalar<MyStruct>::value);
    TEST(std::is_class<MyStruct>::value);
    TEST(std::is_compound<MyStruct>::value);
    TEST(std::is_pod<MyStruct>::value);
    TEST(std::has_trivial_default_constructor<MyStruct>::value);
    TEST(!std::is_pod<MyNonPod>::value);
    TEST(!std::has_trivial_default_constructor<MyNonPod>::value);
    TEST(std::is_const<const int>::value);
    TEST(!std::is_volatile<const int>::value);
    TEST(std::rank<int[][4]>::value == 2);
    TEST(std::rank<int[1][2]>::value == 2);
    TEST((std::extent<int[1][2], 0>::value == 1));
    TEST((std::extent<int[1][2], 1>::value == 2));
    TEST((std::extent<int, 3>::value == 0));
    TEST((std::is_base_of<MyStruct, MyNonPod>::value));
    TEST((!std::is_base_of<MyNonPod, MyStruct>::value));
    TEST((std::is_same<std::add_rvalue_reference<int>::type, int&&>::value));
    TEST((std::is_same<std::add_rvalue_reference<int&>::type, int&>::value));
    TEST((std::is_same<std::add_rvalue_reference<int&&>::type, int&&>::value));
    TEST((std::is_convertible<int&&, int  >::value == true));
    TEST((std::is_convertible<int &, int  >::value == true));
    TEST((std::is_convertible<int  , int  >::value == true));
    TEST((std::is_convertible<int&&, int &>::value == false)); // gcc bugs!
    TEST((std::is_convertible<int &, int &>::value == true));
    TEST((std::is_convertible<int  , int &>::value == false)); // gcc bugs!
    TEST((std::is_convertible<int&&, int&&>::value == true));
    TEST((std::is_convertible<int &, int&&>::value == true));
    TEST((std::is_convertible<int  , int&&>::value == true));
    TEST((std::is_convertible<int&&, float  >::value == true));
    TEST((std::is_convertible<int &, float  >::value == true));
    TEST((std::is_convertible<int  , float  >::value == true));
    TEST((std::is_convertible<int&&, float &>::value == false));
    TEST((std::is_convertible<int &, float &>::value == false));
    TEST((std::is_convertible<int  , float &>::value == false));
    TEST((std::is_convertible<int&&, float&&>::value == true));
    TEST((std::is_convertible<int &, float&&>::value == true));
    TEST((std::is_convertible<int  , float&&>::value == true));
    TEST((std::is_same<std::make_unsigned<int>::type, unsigned int>::value));
    TEST((std::is_same<std::make_signed<unsigned char>::type, signed char>::value));
    TEST((std::is_same<std::make_signed<const unsigned short>::type, const signed short>::value));
    TEST((std::is_same<std::remove_extent<int[1][2]>::type, int[2]>::value));
    TEST((std::is_same<std::remove_all_extents<const unsigned int[1][2]>::type, const unsigned int>::value));
    TEST((std::is_same<std::common_type<char, unsigned int>::type, unsigned int>::value));

    {
        MyMove a;
        a.x = 123;
        TEST(a.x == 123);
        MyMove b(std::move(a));
        TEST(b.x == 123);
        TEST(a.x == 0);
        a = std::move(b);
        TEST(a.x == 123);
        TEST(b.x == 0);
    }

    {
        // try and break the vector :D
        std::vector<int> a, b;
        const std::vector<int> &r = a;

        a.push_back(1);
        a.push_back(2);
        a.push_back(3);

        {
            std::vector<int>::const_reverse_iterator i = r.rbegin();
            TEST(i != r.rend()); TEST(*i == 3); ++i;
            TEST(i != r.rend()); TEST(*i == 2); ++i;
            TEST(i != r.rend()); TEST(*i == 1); ++i;
            TEST(i == r.rend());
        }

        TEST(a[1] == 2);
        TEST(a.at(0) == 1);
        TEST(a.rbegin()[1] == 2);
        TEST(a.front() == *a.begin());
        TEST(a.back() == a[a.size() - 1]);

        bool threw = false;
        try {
            a.at(3);
        } catch (std::out_of_range &e){
            threw = true;
        }
        TEST(threw);

        TEST(&(b = a) == &b);

        {
            std::vector<int>::iterator i = a.begin();
            TEST(i != r.end()); TEST(*i == 1); ++i;
            TEST(i != r.end()); TEST(*i == 2); ++i;
            TEST(i != r.end()); TEST(*i == 3); ++i;
            TEST(i == r.end());
        }

        b.push_back(4);
        b.push_back(5);
        TEST(b.size() == 5);
        TEST(a.size() == 3);
        b[4] = 7;

        TEST_SEQ(b, 5, /**/ 1, 2, 3, 4, 7);

        b.insert(b.begin() + 3, 3, 10);
        TEST_SEQ(b, 8, /**/ 1, 2, 3, 10, 10, 10, 4, 7);
        b.erase(b.begin() + 1);
        TEST_SEQ(b, 7, /**/ 1, 3, 10, 10, 10, 4, 7);
        b.erase(b.begin() + 1, b.begin() + 4);
        TEST_SEQ(b, 4, /**/ 1, 10, 4, 7);

        {
            a.assign(5, 2);
            TEST_SEQ(a, 5, /**/ 2, 2, 2, 2, 2);
            std::vector<int> tmp(5, 2);
            TEST_SEQ(tmp, 5, /**/ 2, 2, 2, 2, 2);
        }
    }

#ifdef TEST_EXT
    {
        typedef std::vector<int, std::allocator<int>, true> vec_t;
        vec_t a;
        a.push_back(1);
        a.push_back(2);
        a.push_back(3);
        TEST(a.size() == 3);

        bool threw = false;
        try {
            *a.begin();
        } catch (std::out_of_range &e){
            threw = true;
        }
        TEST(!threw);

        threw = false;
        try {
          *a.end();
        } catch (std::out_of_range &e){
          threw = true;
        }
        TEST(threw);

        vec_t::iterator i;
        threw = false;
        try {
            *i;
        } catch (std::logic_error &e){
            threw = true;
        }
        TEST(threw);

        i = a.begin();
        threw = false;
        try {
            i += 3;
        } catch (std::out_of_range &e){
            threw = true;
        }
        TEST(!threw);

        threw = false;
        try {
            i -= 4;
        } catch (std::out_of_range &e){
            threw = true;
        }
        TEST(threw);

        threw = false;
        try {
            i += 1; // previous operator will not have changed the iterator
        } catch (std::out_of_range &e){
            threw = true;
        }
        TEST(threw);
    }
#endif

    {
        std::list<int> a, b;
        const std::list<int> &r = a;

        a.push_back(1);
        a.push_back(2);
        a.push_back(3);

        {
            auto i = r.rbegin();
            TEST(i != r.rend()); TEST(*i == 3); ++i;
            TEST(i != r.rend()); TEST(*i == 2); ++i;
            TEST(i != r.rend()); TEST(*i == 1); ++i;
            TEST(i == r.rend());
        }

        TEST(a.front() == *a.begin());
        {
            auto i = a.end();
            TEST(a.back() == *--i);
        }

        TEST(&(b = a) == &b);

        b.push_back(4);
        b.push_back(5);
        b.push_front(-1);
        b.push_front(-2);
        TEST(b.size() == 7);
        TEST(a.size() == 3);
        TEST_SEQ(b, 7, /**/ -2, -1, 1, 2, 3, 4, 5);
        {
            auto i = b.begin();
            advance(i, 3);
            b.insert(i, 3, 10);
            TEST_SEQ(b, 10, /**/ -2, -1, 1, 10, 10, 10, 2, 3, 4, 5);
            i = b.begin();
            advance(i, 4);
            auto j = i;
            advance(j, 3);
            b.erase(i, j);
            TEST_SEQ(b, 7, /**/ -2, -1, 1, 10, 3, 4, 5);
            i = b.end();
            advance(i, -3);
            b.erase(i);
            TEST_SEQ(b, 6, /**/ -2, -1, 1, 10, 4, 5);
        }

        b.reverse();
        TEST_SEQ(b, 6, /**/ 5, 4, 10, 1, -1, -2);
        // TODO: test move constructor
        // TODO: test splice
        // TODO: test unique
    }

#ifdef TEST_EXT
    {
        typedef std::list<int, std::allocator<int>, true> list_t;
        list_t a;
        a.push_back(1);
        a.push_back(2);
        a.push_back(3);

        bool threw = false;
        try {
            *a.begin();
        } catch (std::out_of_range &e){
            threw = true;
        }
        TEST(!threw);

        threw = false;
        try {
            *a.end();
        } catch (std::out_of_range &e){
            threw = true;
        }
        TEST(threw);

        list_t::iterator i;
        threw = false;
        try {
            *i;
        } catch (std::logic_error &e){
            threw = true;
        }
        TEST(threw);

        i = a.begin();
        threw = false;
        try {
            --i;
        } catch (std::out_of_range &e){
            threw = true;
        }
        TEST(threw);

        i = a.begin();
        threw = false;
        try {
            std::advance(i, 3);
        } catch (std::out_of_range &e){
            threw = true;
        }
        TEST(!threw);

        i = a.end();
        threw = false;
        try {
            ++i;
        } catch (std::out_of_range &e){
            threw = true;
        }
        TEST(threw);
    }
#endif

    printf("\x1B[0;32m%d/%d tests passed\n\x1B[0m", test_counter - fail_counter, test_counter);
    return 0;
}
